# Sensirion SEN50/SEN55 control and example project

## Project contents

- boards: contains an overlay file to enable the i2c2 interface on pins 30 (SCL) and 31 (SDA) of the nrf9160DK
- sen5x: sen5x.h header file with the functions to readout a SEN50 or a SEN55 over I2C
- src: sen5x.c source code with the implementations of the functions in sen5x.h
- prj.conf: Zephyr project config to enable the I2C and the PINCONTROL 
- CMakeLists.txt 

## How to compile this project

0. Setup the environment 
```
source sacaqmsw/setup.sh
```

1. Clone this project inside the sacaqmsw folder
``` 
cd sacaqmsw
git clone https://gitlab.cern.ch/sacaqm/sen5x.git
```

2. Make a build directory
```
mkdir build
```

3. Change to the build directory
```
cd build
```

4. Config the release for building
```
cmake ../sen5x
```

5. Compile the software
```
make 
```

6. Flash to the board 
```
FLASH
```


