#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h> 
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/i2c.h>
#include <zephyr/devicetree.h>
#include <stdio.h>   
#include <string.h>  
#include <sen5x/sen5x.h>

#define LED0_NODE DT_ALIAS(led0)
static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);

void blink(){
	gpio_pin_set_dt(&led,1);
	k_msleep(1000);
	gpio_pin_set_dt(&led,0);
}
	
int main(void) {

	printk("Welcome to IOTPM\n");

	printk("Init LED\n");
	device_is_ready(led.port);
	gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	gpio_pin_set_dt(&led,GPIO_INT_LEVEL_LOW);
	
	printk("Init device\n");
  sen5x_init(DEVICE_DT_GET(DT_NODELABEL(i2c2)));
	blink();
	
	printk("Reset device\n");
	sen5x_reset();
	blink();
	
	printk("Get serial number\n");
	char serial[32];
	sen5x_get_serial(serial,sizeof(serial));
	printk("serial: %s\n",serial);
	blink();

	printk("Get name\n");
	char name[32];
	sen5x_get_name(name,32);
	printk("name: %s\n",name);
	blink();
		
	printk("Get version\n");
	struct sen5x_version pmv;
	sen5x_get_version(&pmv);
	printk("FW: %u.%u.%u\n", pmv.firmware_major,pmv.firmware_minor, pmv.firmware_debug);
	printk("HW: %u.%u\n", pmv.hardware_major,pmv.hardware_minor);
	printk("PR: %u.%u\n", pmv.protocol_major,pmv.protocol_minor);
	blink();
	
	
	printk("Start measurement\n");
	sen5x_start();
	blink();

	struct sen5x_measurement pmm;
	
	while(true){
		
		k_msleep(5000);
		
		sen5x_get_measurement(&pmm);
 		blink();
    printk("Mass concentration pm1p0: %i ug/m3\n", (int32_t)(pmm.mass_concentration_pm1p0 / 10.0f));
    printk("Mass concentration pm2p5: %i ug/m3\n", (int32_t)(pmm.mass_concentration_pm2p5 / 10.0f));
    printk("Mass concentration pm4p0: %i ug/m3\n", (int32_t)(pmm.mass_concentration_pm4p0 / 10.0f));
    printk("Mass concentration pm10p0: %i ug/m3\n", (int32_t)(pmm.mass_concentration_pm10p0 / 10.0f));
    printk("Ambient humidity: %i %%RH\n", (int32_t)(pmm.ambient_humidity / 100.0f));
    printk("Ambient temperature: %i C\n", (int32_t)(pmm.ambient_temperature / 200.0f));
    printk("Voc index: %i\n", (int32_t)(pmm.voc_index / 10.0f));
    printk("Nox index: %i\n", (int32_t)(pmm.nox_index / 10.0f));
 		
		
	}
	
  return 0; 
}
